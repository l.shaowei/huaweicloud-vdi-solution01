### Credentials received from terraform.tfvars file ###

variable "ak" {}
variable "sk" {}

### Application Environment variables ### 
variable "app_name" {
    default = "VDI"
}

variable "environment" {
  default = "customerA"
}

variable "time_zone" {
  default = "UTC-05:00"
}

### Region and Availability zone variables ###

variable "region" {
#    default = "la-south-2"
#    default = "sa-brazil-1"
    default = "la-north-2"
}

variable "number_of_azs" {
  default = "1"
}

variable "availability_zone1" {
#    default = "la-south-2a"
#    default = "sa-brazil-1a"
    default = "la-north-2a"
}

variable "availability_zone2" {
#    default = "la-south-2b"
#    default = "sa-brazil-1b"
    default = "la-north-2a"
}

### Network variables ###
## VPC ACCESS ##
variable "vpc_name_access" {
    default = "vpc_access"
}

variable "vpc_cidr_access" {
    default = "10.10.0.0/16"
}

variable "subnet_name_access" {
    default = "subnet_access"
}

variable "subnet_cidr_access" {
    default = "10.10.1.0/24"
}

variable "subnet_gateway_ip_access" {
    default = "10.10.1.1"
}

variable "security_group_name_access" {
    default = "sg_access"
}

variable "sg_ingress_rules_access" {
    type        = map(map(any))
    default     = {
        rule1 = {from=22, to=22, proto="tcp", cidr="0.0.0.0/0", desc="SSH Remotely Login from Internet for Linux"}
        rule2 = {from=3389, to=3389, proto="tcp", cidr="0.0.0.0/0", desc="RDP Remotely Login from Internet for Windows"}
        rule3 = {from=80, to=80, proto="tcp", cidr="0.0.0.0/0", desc="Access Webserver HTTP from Internet"}
        rule4 = {from=443, to=443, proto="tcp", cidr="0.0.0.0/0", desc="Access Webserver HTTPs from Internet"}
        rule5 = {from=3306, to=3306, proto="tcp", cidr="10.10.0.0/16", desc="Access RDS from the VPC CIDR"}
        rule6 = {from=6379, to=6379, proto="tcp", cidr="10.10.0.0/16", desc="Access VPC from the VPC CIDR"}
    }
}

## VPC platform ##
variable "vpc_name_platform" {
    default = "vpc_platform"
}

variable "vpc_cidr_platform" {
    default = "172.16.0.0/12"
}

variable "subnet_name_platform" {
    default = "subnet_platform"
}

variable "subnet_cidr_platform" {
    default = "172.16.1.0/24"
}

variable "subnet_gateway_ip_platform" {
    default = "172.16.1.1"
}

variable "security_group_name_platform" {
    default = "sg_platform"
}

variable "sg_ingress_rules_platform" {
    type        = map(map(any))
    default     = {
        rule1 = {from=22, to=22, proto="tcp", cidr="0.0.0.0/0", desc="SSH Remotely Login from Internet for Linux"}
        rule2 = {from=3389, to=3389, proto="tcp", cidr="0.0.0.0/0", desc="RDP Remotely Login from Internet for Windows"}
        rule3 = {from=80, to=80, proto="tcp", cidr="0.0.0.0/0", desc="Access Webserver HTTP from Internet"}
        rule4 = {from=443, to=443, proto="tcp", cidr="0.0.0.0/0", desc="Access Webserver HTTPs from Internet"}
        rule5 = {from=3306, to=3306, proto="tcp", cidr="10.10.0.0/16", desc="Access RDS from the VPC CIDR"}
        rule6 = {from=6379, to=6379, proto="tcp", cidr="10.10.0.0/16", desc="Access VPC from the VPC CIDR"}
    }
}

## VPC vdi ##
variable "vpc_name_vdi" {
    default = "vpc_vdi"
}

variable "vpc_cidr_vdi" {
    default = "192.168.0.0/16"
}

variable "subnet_name_vdi" {
    default = "subnet_vdi"
}

variable "subnet_cidr_vdi" {
    default = "192.168.1.0/24"
}

variable "subnet_gateway_ip_vdi" {
    default = "192.168.1.1"
}

variable "security_group_name_vdi" {
    default = "sg_vdi"
}

variable "sg_ingress_rules_vdi" {
    type        = map(map(any))
    default     = {
        rule1 = {from=22, to=22, proto="tcp", cidr="0.0.0.0/0", desc="SSH Remotely Login from Internet for Linux"}
        rule2 = {from=3389, to=3389, proto="tcp", cidr="0.0.0.0/0", desc="RDP Remotely Login from Internet for Windows"}
        rule3 = {from=80, to=80, proto="tcp", cidr="0.0.0.0/0", desc="Access Webserver HTTP from Internet"}
        rule4 = {from=443, to=443, proto="tcp", cidr="0.0.0.0/0", desc="Access Webserver HTTPs from Internet"}
        rule5 = {from=3306, to=3306, proto="tcp", cidr="10.10.0.0/16", desc="Access RDS from the VPC CIDR"}
        rule6 = {from=6379, to=6379, proto="tcp", cidr="10.10.0.0/16", desc="Access VPC from the VPC CIDR"}
    }
}


### ECS Instance variables ### 

## 1. ADC Application ##

variable "sub_app_name_adc" {
    default = "ADC"
}

variable "ecs_count_adc" {
    default = "2"
}

variable "ecs_generation_adc" {
  default = "s6"
}

## Specifies the number of vCPUs in the ECS flavor ##
variable "ecs_cpu_core_count_adc" {
  default = "1"
}

## Specifies the memory size(GB) in the ECS flavor ##
variable "ecs_memory_size_adc" {
  default = "2"
}

## Specifies the Public Image Name ##
variable "ecs_image_name_adc" {
  default = "Debian 10.0.0 64bit"
}

variable "ecs_image_type_adc" {
  default = "public"
}

## Specifies the system disk type of the instance ##
## SAS  : High I/O disk type ##
## GPSSD: General purpose SSD disk type ##
## SSD  : Ultra-high I/O disk type ##
## ESSD : Extreme SSD type, Santiago Region supports ##
variable "ecs_sysdisk_type_adc" {
  default = "SAS"
}

variable "ecs_sysdisk_size_adc" {
  default = "40"
}

## Numbers of Datadisk ##
variable "ecs_datadisk_number_adc" {
  default = "0"
}

variable "ecs_datadisk_type_adc" {
  default = "GPSSD"
}

variable "ecs_datadisk_size_adc" {
  default = "100"
}

## AD Application ##

variable "sub_app_name_ad" {
    default = "AD"
}

variable "ecs_count_ad" {
    default = "3"
}

variable "ecs_generation_ad" {
  default = "s6"
}

## Specifies the number of vCPUs in the ECS flavor ##
variable "ecs_cpu_core_count_ad" {
  default = "1"
}

## Specifies the memory size(GB) in the ECS flavor ##
variable "ecs_memory_size_ad" {
  default = "2"
}

## Specifies the Public Image Name ##
variable "ecs_image_name_ad" {
  default = "Debian 10.0.0 64bit"
}

variable "ecs_image_type_ad" {
  default = "public"
}

## Specifies the system disk type of the instance ##
## SAS  : High I/O disk type ##
## GPSSD: General purpose SSD disk type ##
## SSD  : Ultra-high I/O disk type ##
## ESSD : Extreme SSD type, Santiago Region supports ##
variable "ecs_sysdisk_type_ad" {
  default = "SAS"
}

variable "ecs_sysdisk_size_ad" {
  default = "40"
}

## Numbers of Datadisk ##
variable "ecs_datadisk_number_ad" {
  default = "0"
}

variable "ecs_datadisk_type_ad" {
  default = "GPSSD"
}

variable "ecs_datadisk_size_ad" {
  default = "100"
}

## ECS Common variables ##

variable "ecs_admin_pass" {
  default = "ADq1@39a1!"
}

variable "ecs_attach_eip" {
  default = false
}

variable "eip_bandwidth_size" {
  default = "50"
}

### CBR variables ###

## Specifies the CBR vault sapacity for ECS, in GB. The valid value range is 1 to 10,485,760 ##
variable "cbr_ecs_vault_size" {
    default = "100"
}
 
##  Specifies the backup time TIMEZONE: UTC-0 ##
variable "cbr_ecs_backup_starttime" {
  default = "05:00"
}

## Specifies the duration (in days) for CBR ECS retained backups. The value ranges from 2 to 99,999 ##
variable "ecs_retention_time_period" {
  default = "90"
}

### SMN variable ###

variable "alarm_email_address" {
    default = "abcd@abc.com"
}

### SCM Certificate variables ###
variable "cert_path" {
  default = "/opt/certpath/"
}

### ELB common varialbes ###
variable "elb_bandwidth_size" {
  default = "50"
}

## Variables for Shared ELB ##

variable "elb_attach_eip" {
  default = true
}

## Varialbes for Dedicated ELB ##
## ELB Flavor and Max connections Mapping ##
## Small   I  - Maximum Connections:  200000 ##
## Small   II - Maximum Connections:  400000 ##
## Medium  I  - Maximum Connections:  800000 ##
## Medium  II - Maximum Connections: 2000000 ##
## Large   I  - Maximum Connections: 4000000 ##
## Large   II - Maximum Connections: 8000000 ##

variable "elb_max_connections" {
  default = "200000"
}

### NATGATEWAY variables ###

## NATGATEWAY TYPE 1: Small Type, 2: Medium Type, 3: Large Type, 4: Extra Type ##
variable "nat_gateway_type" {
  default = "1"
}

## NATGATEWAY BANDWIDTH SIZE, unit Mbit/s ##
variable "nat_gateway_bandwidth_size" {
  default = "100"
}

#### Autoscaling variables ###
### ADC Application ##
#
#variable "ecs_generation_adc" {
#  default = "s6"
#}
#
#variable "cpu_core_count_adc" {
#  default = "2"
#}
#
#variable "memory_size_adc" {
#  default = "4"
#}
#
#variable "ecs_sysdisk_size_adc" {
#  default = "40"
#}
#
#variable "ecs_sysdisk_type_adc" {
#  default = "GPSSD"
#}
#
### Specifies the Public Image Name ##
#variable "ecs_image_name_adc" {
#  default = "Debian 10.0.0 64bit"
#}
#
#variable "ecs_image_type_adc" {
#  default = "public"
#}
#
#
### The expected number of instances.  ##
#variable "desire_instance_number_adc" {
#  default = "2"
#}
#
### The minimum number of instances. ##
#variable "min_instance_number_adc" {
#  default = "2"
#}
### The maximum number of instances. ##
#variable "max_instance_number_adc" {
#  default = "2"
#}
