### Provider Huawei Cloud and Credentials ## 
terraform {
  required_providers {
    huaweicloud = {
      source = "huaweicloud/huaweicloud"
      version = "1.38.2"
    }
  }
}

provider "huaweicloud" {
  region      = "${var.region}"
  access_key  = "${var.ak}"
  secret_key  = "${var.sk}"
}


################ Declaring Module VPC Working #### DONE ##############

module "eps" {
  source              = "../modules/eps/"
  count               = 0
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
}

### 3 VPC & 3 Subnets & 3 SGs ##
## VPC ACCESS ##
module "vpc_access" {
  source              = "../modules/vpc/"
  depends_on          = [module.eps]
  region              = "${var.region}"
  vpc_name            = "${var.vpc_name_access}"
  vpc_cidr            = "${var.vpc_cidr_access}"
  subnet_name         = "${var.subnet_name_access}"
  subnet_cidr         = "${var.subnet_cidr_access}"
  subnet_gateway_ip   = "${var.subnet_gateway_ip_access}"  
  app_name            = "${var.app_name}"
  environment	      = "${var.environment}"
  security_group_name = "${var.security_group_name_access}"
  sg_ingress_rules    = "${var.sg_ingress_rules_access}"
}

## VPC PLATFORM ##
module "vpc_platform" {
  source              = "../modules/vpc/"
  depends_on          = [module.eps]
  region              = "${var.region}"
  vpc_name            = "${var.vpc_name_platform}"
  vpc_cidr            = "${var.vpc_cidr_platform}"
  subnet_name         = "${var.subnet_name_platform}"
  subnet_cidr         = "${var.subnet_cidr_platform}"
  subnet_gateway_ip   = "${var.subnet_gateway_ip_platform}"
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
  security_group_name = "${var.security_group_name_platform}"
  sg_ingress_rules    = "${var.sg_ingress_rules_platform}"
}

## VPC VDI ##
module "vpc_vdi" {
  source              = "../modules/vpc/"
  depends_on          = [module.eps]
  region              = "${var.region}"
  vpc_name            = "${var.vpc_name_vdi}"
  vpc_cidr            = "${var.vpc_cidr_vdi}"
  subnet_name         = "${var.subnet_name_vdi}"
  subnet_cidr         = "${var.subnet_cidr_vdi}"
  subnet_gateway_ip   = "${var.subnet_gateway_ip_vdi}"
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
  security_group_name = "${var.security_group_name_vdi}"
  sg_ingress_rules    = "${var.sg_ingress_rules_vdi}"
}


## VPC_access to VPC_platform ##
module "vpc_peering01" {
  source              = "../modules/vpc_peering/"
  depends_on          = [module.vpc_access,module.vpc_platform]
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
  vpc_name1           = "${var.vpc_name_access}"
  vpc_name2           = "${var.vpc_name_platform}"
}

## VPC_access to VPC_platform ##
module "vpc_peering02" {
  source              = "../modules/vpc_peering/"
  depends_on          = [module.vpc_access,module.vpc_vdi]
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
  vpc_name1           = "${var.vpc_name_access}"
  vpc_name2           = "${var.vpc_name_vdi}"
}


################## Declaring Module ECS for Image Creation Working ###### DONE ######### 
###  9 types of ECS ###

## ADC Windows ##
module "ecs_adc" {
  source              = "../modules/ecs/"
  count               = "${var.ecs_count_adc}"
  depends_on          = [module.vpc_access]
  region              = "${var.region}"
  number_of_azs       = "${var.number_of_azs}"
  availability_zone   = "${var.availability_zone1}"
  availability_zone1  = "${var.availability_zone1}"
  availability_zone2  = "${var.availability_zone2}"
  subnet_name         = "${var.subnet_name_access}"
  security_group_name = "${var.security_group_name_platform}"
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
  sub_app_name        = "${var.sub_app_name_adc}-${count.index + 1}"
  ecs_image_name      = "${var.ecs_image_name_adc}"
  ecs_image_type      = "${var.ecs_image_type_adc}"
  ecs_generation      = "${var.ecs_generation_adc}"
  cpu_core_count      = "${var.ecs_cpu_core_count_adc}"
  memory_size         = "${var.ecs_memory_size_adc}"
  ecs_sysdisk_type    = "${var.ecs_sysdisk_type_adc}"
  ecs_sysdisk_size    = "${var.ecs_sysdisk_size_adc}"
  ecs_datadisk_number = "${var.ecs_datadisk_number_adc}"
  ecs_datadisk_type   = "${var.ecs_datadisk_type_adc}"
  ecs_datadisk_size   = "${var.ecs_datadisk_size_adc}"
  ecs_attach_eip      = "${var.ecs_attach_eip}"
  eip_bandwidth_size  = "${var.eip_bandwidth_size}"
  ecs_admin_pass      = "${var.ecs_admin_pass}"
}

### AD Windows ##
module "ecs_ad" {
  source              = "../modules/ecs/"
  count               = "${var.ecs_count_ad}"
  depends_on          = [module.vpc_platform]
  region              = "${var.region}"
  number_of_azs       = "${var.number_of_azs}"
  availability_zone   = "${var.availability_zone1}"
  availability_zone1  = "${var.availability_zone1}"
  availability_zone2  = "${var.availability_zone2}"
  subnet_name         = "${var.subnet_name_platform}"
  security_group_name = "${var.security_group_name_platform}"
  app_name            = "${var.app_name}"
  environment	      = "${var.environment}"
  sub_app_name        = "${var.sub_app_name_ad}-${count.index + 1}"
  ecs_image_name      = "${var.ecs_image_name_ad}"
  ecs_image_type      = "${var.ecs_image_type_ad}"
  ecs_generation      = "${var.ecs_generation_ad}"
  cpu_core_count      = "${var.ecs_cpu_core_count_ad}"
  memory_size         = "${var.ecs_memory_size_ad}"
  ecs_sysdisk_type    = "${var.ecs_sysdisk_type_ad}"
  ecs_sysdisk_size    = "${var.ecs_sysdisk_size_ad}"
  ecs_datadisk_number = "${var.ecs_datadisk_number_ad}"
  ecs_datadisk_type   = "${var.ecs_datadisk_type_ad}"
  ecs_datadisk_size   = "${var.ecs_datadisk_size_ad}"
  ecs_attach_eip      = "${var.ecs_attach_eip}"
  eip_bandwidth_size  = "${var.eip_bandwidth_size}"
  ecs_admin_pass      = "${var.ecs_admin_pass}"
}

# DDC Windows ##
## SF Windows ##
## LIC Windows ##
## SQL Windows ##
## SR Windows ##
## ADM Windows ##
## Standard VDI Windows ##
## Hight Performance VDI Windows ##

#################### Declaring Module CTS Working ##### DONE ########## 

module "cts" {
  source              = "../modules/cts/"
}

#################### Declaring Module SMN Working ##### DONE ########## 

module "smn" {
  source              = "../modules/smn/"
  depends_on          = [module.eps]
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
  alarm_email_address = "${var.alarm_email_address}"
}

##################### Declaring Module CES Working ##### DONE ########## 

module "ces" {
  source              = "../modules/ces/"
  depends_on          = [module.smn]
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
}

##################### Declaring Module CBR For ECS Working ##### DONE ########## 

module "cbr_ecs" {
  source                      = "../modules/cbr_ecs/"
  app_name                    = "${var.app_name}"
  environment                 = "${var.environment}"
  region                      = "${var.region}"
  time_zone                   = "${var.time_zone}"
  cbr_ecs_vault_size          = "${var.cbr_ecs_vault_size}"
  retention_time_period       = "${var.ecs_retention_time_period}"
  cbr_ecs_backup_starttime    = "${var.cbr_ecs_backup_starttime}"
}

################# Declaring Module SCM Working ####### DONE ########

module "scm" {
  source               = "../modules/scm/"
  region               = "${var.region}"
  app_name             = "${var.app_name}"
  environment          = "${var.environment}"
}

################## Declaring Module Dedicated ELB Working #### DONE ##############
 ## Region: la-south-2 / sa-brazil-1 / la-north-2 support Dedicated ELB ##

### VPC_ACCESS ###
module "dedicated_elb" {
  source              = "../modules/dedicated_elb/"
  depends_on          = [module.vpc_access,module.ecs_adc]
  app_name            = "${var.app_name}"
  environment         = "${var.environment}"
  region              = "${var.region}"
  number_of_azs       = "${var.number_of_azs}"
  availability_zone   = "${var.availability_zone1}"
  availability_zone1  = "${var.availability_zone1}"
  availability_zone2  = "${var.availability_zone2}"
  vpc_name            = "${var.vpc_name_access}"
  subnet_name         = "${var.subnet_name_access}"
  elb_bandwidth_size  = "${var.elb_bandwidth_size}"
  elb_max_connections = "${var.elb_max_connections}"
  ecs_name_member01   = "${var.sub_app_name_adc}-1"
  ecs_name_member02   = "${var.sub_app_name_adc}-2"
}

################# Declaring Module NATGATEWAY Working #### DONE ##############

module "nat_gateway" {
  source                     = "../modules/nat_gateway/"
  depends_on                 = [module.vpc_vdi]
  app_name                   = "${var.app_name}"
  environment                = "${var.environment}"
  region                     = "${var.region}"
  vpc_name                   = "${var.vpc_name_vdi}"
  subnet_name                = "${var.subnet_name_vdi}"
  nat_gateway_type           = "${var.nat_gateway_type}"
  nat_gateway_bandwidth_size = "${var.nat_gateway_bandwidth_size}"
}
