### Provider Huawei Cloud ##

terraform {
  required_providers {
    huaweicloud = {
      source = "huaweicloud/huaweicloud"
      version = "1.38.2"
    }
  }
}

data "huaweicloud_vpc" "vpc_main" {
  name        = "${var.vpc_name1}"
}

data "huaweicloud_vpc" "vpc_peer" {
  name        = "${var.vpc_name2}"
}

data "huaweicloud_vpc_subnet_ids" "subnet_ids_main" {
  vpc_id      = "${data.huaweicloud_vpc.vpc_main.id}"
}

data "huaweicloud_vpc_subnet_ids" "subnet_ids_peer" {
  vpc_id      = "${data.huaweicloud_vpc.vpc_peer.id}"
}

### Create VPC Peering ###
resource "huaweicloud_vpc_peering_connection" "peering_main" {
  name        = "Peering_${var.vpc_name1}_${var.vpc_name2}"
  vpc_id      = "${data.huaweicloud_vpc.vpc_main.id}"
  peer_vpc_id = "${data.huaweicloud_vpc.vpc_peer.id}"
}

### Query the default route in VPC name1 ###
data "huaweicloud_vpc_route_table" "vpc_main_default" {
  vpc_id         = "${data.huaweicloud_vpc.vpc_main.id}"
}

### Create route in vpc name1 ###
resource "huaweicloud_vpc_route" "vpc_route_main" {
  description    = "Route from ${var.vpc_name1} to ${var.vpc_name2}"
  vpc_id         = "${data.huaweicloud_vpc.vpc_main.id}"
  route_table_id = "${data.huaweicloud_vpc_route_table.vpc_main_default.id}"
  destination    = "${data.huaweicloud_vpc.vpc_peer.cidr}"
  type           = "peering"
  nexthop        = "${huaweicloud_vpc_peering_connection.peering_main.id}"
}

### Query the default route in VPC name2 ###
data "huaweicloud_vpc_route_table" "vpc_peer_default" {
  vpc_id         = "${data.huaweicloud_vpc.vpc_peer.id}"
}

### Create route in vpc name2 ###
resource "huaweicloud_vpc_route" "vpc_route_peer" {
  description    = "Route from ${var.vpc_name2} to ${var.vpc_name1}"
  vpc_id         = "${data.huaweicloud_vpc.vpc_peer.id}"
  route_table_id = "${data.huaweicloud_vpc_route_table.vpc_peer_default.id}"
  destination    = "${data.huaweicloud_vpc.vpc_main.cidr}"
  type           = "peering"
  nexthop        = "${huaweicloud_vpc_peering_connection.peering_main.id}"
}
