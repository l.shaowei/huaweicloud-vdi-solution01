## application environment variables ##

variable "app_name" {
}

variable "environment" {
}

variable "tags" {
  default = {
  }
}

## Region and Availability zone variables ##

## Network variables ##

variable "vpc_name1" {
}

variable "vpc_name2" {
}

## Security Group variable ##
