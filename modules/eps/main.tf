### Provider Huawei Cloud ##

terraform {
  required_providers {
    huaweicloud = {
      source = "huaweicloud/huaweicloud"
      version = "1.38.2"
    }
  }
}

#data "huaweicloud_enterprise_project" "enterprise_project" {
#  name = "${var.app_name}-${var.environment}"
#}
#
#resource "huaweicloud_enterprise_project" "enterprise_project" {
#  count       = data.huaweicloud_enterprise_project.enterprise_project.id == "" ? 1 : 0
#  name        = "${var.app_name}-${var.environment}"
#  description = "${var.app_name} ${var.environment} Enterprise Project"
#  enable      = true
#}

resource "huaweicloud_enterprise_project" "enterprise_project" {
  name        = "${var.app_name}-${var.environment}"
  description = "${var.app_name} ${var.environment} Enterprise Project"
  enable      = true
}
